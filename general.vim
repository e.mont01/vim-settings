syntax enable
let mapleader="\<Space>"
let maplocalleader = ","
set number
set nocompatible
set clipboard+=unnamedplus

try
  colorscheme gruvbox
  " set background=dark
catch /^Vim\%((\a\+)\)\=:E185/
  :silent!
endtry

" TextEdit might fail if hidden is not set.
set hidden

" Some servers have issues with backup files, see #649.
set nobackup
set nowritebackup

" Give more space for displaying messages.
set cmdheight=2

" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
set updatetime=300

" Don't pass messages to |ins-completion-menu|.
set shortmess+=c

" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved.
set signcolumn=yes

" Add `:Format` command to format current buffer.
"command! -nargs=0 Format :call CocAction('format')

" Add `:Fold` command to fold current buffer.
"command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" Add `:OR` command for organize imports of the current buffer.
"command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')

" Add (Neo)Vim's native statusline support.
" NOTE: Please see `:h coc-status` for integrations with external plugins that
" provide custom statusline: lightline.vim, vim-airline.
"set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

" set exrc " enable project speficific vimrc
set secure "ensures that shell, autocmd and write commands are not allowed in the .vimrc file that was found in the current directory, no need to take risks.
set encoding=utf-8

" #####################################################################
" customizing vim
set swapfile
set undofile
set backupdir=~/.vim/backup//
set directory=~/.vim/swap//
set undodir=~/.vim/undo//
set viewdir=~/.vim/views//
set confirm


" Vim UI {
set termguicolors
let &colorcolumn="80,120"

set tabpagemax=15               " Only show 15 tabs

set backspace=indent,eol,start
set linespace=0                 " No extra spaces between rows
set showmatch                   " Show matching brackets/parenthesis
set matchtime=0                 " Tenths of a second to show the matching paren, see 'showmatch'.
set hlsearch                    " Highlight search terms
set incsearch                   " Find as you type search
set ignorecase                  " Case insensitive search
set smartcase                   " Case sensitive when uc present
set winminheight=0              " Windows can be 0 line high
set winminheight=0              " Windows can be 0 line high
set wildmenu                    " Show list instead of just completing
set wildmode=list:longest,full  " Command <Tab> completion, list matches, then longest common part, then all.
set whichwrap=b,s,h,l,<,>,[,]   " Backspace and cursor keys wrap too
set scrolljump=5                " Lines to scroll when cursor leaves screen
set scrolloff=3                 " Minimum lines to keep above and below cursor
set sidescrolloff=5
set foldenable                  " Auto fold code
set foldmethod=indent
" set nofoldenable
set foldlevelstart=99
set showbreak=↪\
set listchars=tab:→\ ,trail:.,extends:›,precedes:‹,nbsp:•,eol:↲ " Highlight problematic whitespace
set list

set history=1000
set autoread
set noerrorbells "Disable beep on errors.
set visualbell   " Flash the screen instead of beeping on errors
set title
set linebreak

set mouse=""
set cursorline                  " Highlight current line
set cursorcolumn                " Highlight current column
highlight clear SignColumn      " SignColumn should match background
highlight clear LineNr          " Current line number row will have same background color in relative mode

" Formatting {{{
set autoindent                 " Indent at the same level of the previous line
filetype plugin indent on      " Smart autoindentation
set tabstop=2                  " An indentation every four columns
set softtabstop=2              " Let backspace delete indent
set shiftwidth=2               " Use indents of 2 spaces
set expandtab                  " Tabs are spaces, not tabs
set nowrap                     " Do not wrap long lines
set nojoinspaces               " Prevents inserting two spaces after punctuation on a join (J)
set splitright                 " Puts new vsplit windows to the right of the current
set splitbelow                 " Puts new split windows to the bottom of the current
"set matchpairs+=<:>           " Match, to be used with %
set pastetoggle=<F12>          " pastetoggle (sane indentation on pastes)
set comments=sl:/*,mb:*,elx:*/ " auto format comment blocks
set formatoptions+=j           " Delete comment character when joining commented lines
" }}}

" Spell checking {{{
set spell
set spelllang=en,es
" }}}

" netrw {{{
let g:netrw_banner = 0
let g:netrw_liststyle = 3
"1 - open files in a new horizontal split
"2 - open files in a new vertical split
"3 - open files in a new tab
"4 - open in previous window
let g:netrw_browse_split = 4
let g:netrw_altv = 1
let g:netrw_winsize = 15
"}}}
