" solve issues with tmux configured with screen-256color
" in .tmux.conf set the following set -g default-terminal "screen-256color"
if exists("$TMUX")
  set t_Co=256
  set notermguicolors
else
  set termguicolors
endif
