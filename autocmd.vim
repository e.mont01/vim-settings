" autocomplete {{{
" hrsh7th/nvim-cmp
autocmd FileType sql,mysql,plsql lua require('cmp').setup.buffer({ sources = {{ name = 'vim-dadbod-completion' }} })
" }}}

"vim files{{{
augroup vim-files
  au!
  autocmd FileType vim setlocal foldmethod=marker
augroup END
" }}}

"filebrowser {{{
augroup netrw
  au!
  au FileType netrw set nolist
augroup END
" }}}

"autorun by format {{{
augroup autorun
  au!
  " au FileType sh noremap <F5> :w!<CR>:!/usr/bin/env sh %<CR>
  " au FileType python noremap <F5> :w!<CR>:!/usr/bin/env python %<CR>

  " https://vimawesome.com/plugin/maven-compiler
  " :autocmd Filetype pom compiler mvn
  " :autocmd Filetype java compiler mvn
  
  " au FileType java no <F5> :make -q exec:java<CR>
  " au FileType java no <F2> :make compile test-compile test<CR>
  " au FileType java no <F3> :make clean compile test-compile test<CR>
  " au FileType pom no <F5> :make -q exec:java<CR>
  " au FileType pom no <F2> :make compile test-compile test<CR>
  " au FileType pom no <F3> :make clean compile test-compile test<CR>
  
  " map <F5> :make compile test-compile<CR>
  " imap <F5> <ESC>:make compile test-compile<CR>
  " map <C-F5> :make clean compile test-compile<CR>
  " imap <C-F5> <ESC>:make clean compile test-compile<CR>
  " map <C-F11> :make -q exec:java<CR>
  " imap <C-F11> :make -q exec:java<CR>
  
  " au FileType java let b:jcommenter_class_author='Eivar Montenegro <e.mont01@gmail.com>'
  " au FileType java let b:jcommenter_file_author='Eivar Montenegro <e.mont01@gmail.com>'
  " au FileType java nmap ¢ :call JCommentWriter()<CR>
augroup END
" }}}

" phpactor {{{
augroup php
  au!
  au FileType php set iskeyword+=$
"  au FileType php nmap <Leader>u :call phpactor#UseAdd()<CR>
"  au FileType php nmap <Leader>mm :call phpactor#ContextMenu()<CR>
"  au FileType php nmap <Leader>nn :call phpactor#Navigate()<CR>
"  au FileType php nmap <Leader>o :call phpactor#GotoDefinition()<CR>
"  au FileType php nmap <Leader>tt :call phpactor#Transform()<CR>
"  au FileType php nmap <Leader>cc :call phpactor#ClassNew()<CR>
"  au FileType php nmap <silent><Leader>ee :call phpactor#ExtractExpression(v:false)<CR>
"  au FileType php vmap <silent><Leader>ee :<C-U>call phpactor#ExtractExpression(v:true)<CR>
"  au FileType php vmap <silent><Leader>em :<C-U>call phpactor#ExtractMethod()<CR>
augroup END
" }}}

"conquer of completion {{{
" augroup coc
"   au!
"   " Setup formatexpr specified filetype(s).
"   au FileType typescript,json setl formatexpr=CocAction('formatSelected')
"   " Update signature help on jump placeholder.
"   au User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')

"   " Highlight the symbol and its references when holding the cursor.
"   au CursorHold * silent call CocActionAsync('highlight')
" augroup end
" }}}

"custom types {{{
augroup custom_types
  au!
  au BufNewFile,BufRead *.i,*.n,*.r,*.s,*.v set filetype=xml
augroup END
" }}}

" HEX edit {{{
" vim -b : edit binary using xxd-format!
augroup Binary
  au!
  au BufReadPre   *.bin let &bin=1
  au BufReadPost  *.bin if &bin | %!xxd
  au BufReadPost  *.bin set ft=xxd | endif
  au BufWritePre  *.bin if &bin | %!xxd -r
  au BufWritePre  *.bin endif
  au BufWritePost *.bin if &bin | %!xxd
  au BufWritePost *.bin set nomod | endif
augroup END
" }}}
