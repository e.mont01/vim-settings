"Plugins{{{
call plug#begin('~/.vim/vimplug')
  Plug 'junegunn/vim-plug'
  "fzf https://github.com/junegunn/fzf/issues/1486
  Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' } | Plug 'junegunn/fzf.vim'

  " TMUX {{{
  Plug 'christoomey/vim-tmux-navigator'
  Plug 'preservim/vimux'
  " }}}

  " Undo tree
  Plug 'simnalamburt/vim-mundo'

  " Collection of color schemes
  Plug 'rafi/awesome-vim-colorschemes'
  Plug 'ajmwagar/vim-deus'
  " Coding standards
  Plug 'editorconfig/editorconfig-vim'
  " visually displaying indent levels
  Plug 'nathanaelkane/vim-indent-guides'
  " Vim script for text filtering and alignment
  Plug 'godlygeek/tabular'
  " highlight recently yanked/copied text
  Plug 'statox/FYT.vim'
  " notifications
  Plug 'rcarriga/nvim-notify'

  " Basic support for .env and Procfile
  Plug 'tpope/vim-dotenv'
  " Repeat.vim remaps . in a way that plugins can tap into it.
  Plug 'tpope/vim-repeat'
  " Better search and replace
  Plug 'tpope/vim-abolish'
  " Comment out lines
  Plug 'tpope/vim-commentary'
  " all about surroundings: parentheses, brackets, quotes, XML tags, and more. 
  Plug 'tpope/vim-surround'
  if has('unix')
    " UNIX shell commands like Delete, Unlink, Move, SudoWrite, etc
    Plug 'tpope/vim-eunuch'
  endif

  " zoom window plugin that uses vim's tabs feature to zoom into a window
  Plug 'troydm/zoomwintab.vim'
  " Quickly close several buffers (bdelete) at once
  Plug 'Asheq/close-buffers.vim'

  " ctags
  Plug 'majutsushi/tagbar'

  " documentation
  Plug 'kkoomen/vim-doge'

  " Better autocompletion/LSP {{{
  " Plug 'neoclide/coc.nvim', {'branch': 'release'}
  " dev AI: codeium
  Plug 'Exafunction/codeium.vim', { 'branch': 'main' }

  Plug 'neovim/nvim-lspconfig'
  Plug 'hrsh7th/cmp-nvim-lsp'
  Plug 'hrsh7th/cmp-buffer'
  Plug 'hrsh7th/cmp-path'
  Plug 'hrsh7th/cmp-cmdline'
  Plug 'hrsh7th/nvim-cmp'
  " }}}

  " Better status bar
  Plug 'itchyny/lightline.vim'

  " A collection of language packs for vim
  Plug 'sheerun/vim-polyglot'
  " Plug 'weakish/rcshell.vim'
  Plug 'vim-scripts/rcshell.vim'

  " Debugger UI
  " luarocks install nvim-dap
  Plug 'mfussenegger/nvim-dap'
  Plug 'nvim-neotest/nvim-nio'
  Plug 'rcarriga/nvim-dap-ui'
  Plug 'mfussenegger/nvim-dap-python'

  " VCS systems {{{
  if has('nvim') || has('patch-8.0.902')
    Plug 'mhinz/vim-signify'
  else
    Plug 'mhinz/vim-signify', {'branch': 'legacy'}
  endif
  " git wrapper
  Plug 'tpope/vim-fugitive'
  " ease your git workflow within vim
  Plug 'jreybert/vimagit'
  " reveals git commit messages under the cursor
  Plug 'rhysd/git-messenger.vim'
  " }}}

  "Neomake is a plugin for vim/Neovim to asynchronously run programs.
  " Plug 'neomake/neomake'
  Plug 'etdev/vim-hexcolor'

  " HTML
  Plug 'mattn/emmet-vim',  { 'for': ['pom', 'html', 'xml', 'blade', 'vue'] }

  Plug 'prettier/vim-prettier', {
  \ 'do': 'yarn install',
  \ 'branch': 'release/1.x',
  \ 'for': [
    \ 'javascript',
    \ 'typescript',
    \ 'css',
    \ 'less',
    \ 'scss',
    \ 'json',
    \ 'graphql',
    \ 'markdown',
    \ 'vue',
    \ 'lua',
    \ 'php',
    \ 'python',
    \ 'ruby',
    \ 'html',
    \ 'swift'
    \ ]
  \ }

  " Database {{{
  " Plug 'nanotee/sqls.nvim'
  Plug 'tpope/vim-dadbod'
  Plug 'kristijanhusak/vim-dadbod-ui'
  Plug 'kristijanhusak/vim-dadbod-completion'
  " }}}

  " Notes {{{
  Plug 'xolox/vim-misc'
  Plug 'xolox/vim-notes'
  " }}}

  " Latex {{{
  Plug 'lervag/vimtex'
  Plug 'xuhdev/vim-latex-live-preview', { 'for': 'tex' }
  " }}}

  " Tasks & todo {{{
  Plug 'vuciv/vim-bujo'
  Plug 'itchyny/calendar.vim'
  " }}}
  
  " Mail {{{
  Plug 'itchyny/calendar.vim'
  " }}}

  " leader map guide
  Plug 'spinks/vim-leader-guide'

  " Lua functions
  Plug 'nvim-lua/plenary.nvim'

  " TDD
  Plug 'janko-m/vim-test'
call plug#end()
"}}}


" lightline {{{
set laststatus=2
let g:lightline = {
    \ 'colorscheme': 'one',
    \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
      \ },
    \ 'component_function': {
      \ 'gitbranch': 'fugitive#head'
      \ },
    \ }
"}}}

" neomake {{{
" Full config: when writing or reading a buffer, and on changes in insert and
" normal mode (after 500ms; no delay when writing).
" call neomake#configure#automake('nrwi', 500)
" }}}


" emmet {{{
"redefine trigger key
let g:user_emmet_leader_key=','
"}}}

" nathanaelkane/vim-indent-guides {{{
let g:indent_guides_enable_on_vim_startup = 1
"}}}

" Git Blame {{{
command! -nargs=* Blame call s:GitBlame()

function! s:GitBlame()
  let cmd = "git blame -w " . bufname("%")
  let nline = line(".") + 1
  botright new
  execute "$read !" . cmd
  execute "normal " . nline . "gg"
  execute "set filetype=perl"
endfunction
"}}}

" nvim-lspconfig {{{
" lua << EOF
" -- Setup language servers.
" require('lspconfig').sqls.setup{
"     on_attach = function(client, bufnr)
"         require('sqls').on_attach(client, bufnr)
"     end
" }
" EOF
" }}}

" Set up nvim-cmp {{{
lua <<EOF
  local cmp = require('cmp')

  cmp.setup({
    snippet = {
      -- REQUIRED - you must specify a snippet engine
      expand = function(args)
        -- vim.fn["vsnip#anonymous"](args.body) -- For `vsnip` users.
        -- require('luasnip').lsp_expand(args.body) -- For `luasnip` users.
        -- require('snippy').expand_snippet(args.body) -- For `snippy` users.
        -- vim.fn["UltiSnips#Anon"](args.body) -- For `ultisnips` users.
        vim.snippet.expand(args.body) -- For native neovim snippets (Neovim v0.10+)
      end,
    },
    window = {
      -- completion = cmp.config.window.bordered(),
      -- documentation = cmp.config.window.bordered(),
    },
    mapping = cmp.mapping.preset.insert({
      ['<C-b>'] = cmp.mapping.scroll_docs(-4),
      ['<C-f>'] = cmp.mapping.scroll_docs(4),
      ['<C-Space>'] = cmp.mapping.complete(),
      ['<C-e>'] = cmp.mapping.abort(),
      ['<CR>'] = cmp.mapping.confirm({ select = true }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
    }),
    sources = cmp.config.sources({
      { name = 'nvim_lsp' },
    }, {
      { name = 'buffer' },
    })
  })

  -- To use git you need to install the plugin petertriho/cmp-git and uncomment lines below
  -- Set configuration for specific filetype.
  --[[ cmp.setup.filetype('gitcommit', {
    sources = cmp.config.sources({
      { name = 'git' },
    }, {
      { name = 'buffer' },
    })
 })
 require("cmp_git").setup() ]]-- 

  -- Use buffer source for `/` and `?` (if you enabled `native_menu`, this won't work anymore).
  cmp.setup.cmdline({ '/', '?' }, {
    mapping = cmp.mapping.preset.cmdline(),
    sources = {
      { name = 'buffer' }
    }
  })

  -- Use cmdline & path source for ':' (if you enabled `native_menu`, this won't work anymore).
  cmp.setup.cmdline(':', {
    mapping = cmp.mapping.preset.cmdline(),
    sources = cmp.config.sources({
      { name = 'path' }
    }, {
      { name = 'cmdline' }
    }),
    matching = { disallow_symbol_nonprefix_matching = false }
  })

  -- Set up lspconfig.
  local capabilities = require('cmp_nvim_lsp').default_capabilities()
  -- Replace <YOUR_LSP_SERVER> with each lsp server you've enabled.
  -- require('lspconfig')['<YOUR_LSP_SERVER>'].setup {
  --   capabilities = capabilities
  -- }
EOF
" }}}
