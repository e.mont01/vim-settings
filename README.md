# Requirements

1. **VIM**: neovim or vim 7.0 or above

2. **vim-plug**: You need to install [vim-plug|https://github.com/junegunn/vim-plug], you can use any other plugin manager but will have to adjust the way plugins are defined.

3. **git**: As indicated in [vim-plug requirements|https://github.com/junegunn/vim-plug/wiki/requirements] "Most of the features should work with Git 1.7 or above. However, installing plugins with tags is known to fail if Git is older than 1.7.10. Git 1.8 or greater is recommended."

# Usage

1. Download this repo using git clone:

```bash
git clone git@gitlab.com:e.mont01/vim-settings.git ~/vim-settings
```

2. configure your editor

  * vim: .vimrc file `ln -sT ~/vim-settings/settings.vim ~/.vimrc`
  * neovim: init.vim file `ln -sT ~/vim-settings/settings.vim ~/.config/nvim/init.vim`

3. Create some necesary directories
```bash
mkdir -p ~/.vim/vimplug
mkdir -p ~/.vim/backup
mkdir -p ~/.vim/swap
mkdir -p ~/.vim/undo
mkdir -p ~/.vim/views
```

Now run you can run vim/neovim and install all defined plugins:
```bash
vim +PlugInstall
```
