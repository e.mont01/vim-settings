" fzf {{{
" map <C-p> :GFiles<CR>
" See more about fzf at https://dev.to/jesseleite/it-s-dangerous-to-vim-alone-take-fzf-2i1i
nmap <leader>/ :Ag<Space>
nmap <leader>f :GFiles<CR>
nmap <leader>F :Files<CR>
nmap <leader>b :Buffers<CR>
nmap <leader>h :History<CR>
nmap <leader>t :BTags<CR>
nmap <leader>T :Tags<CR>
nmap <leader>l :BLines<CR>
nmap <leader>L :Lines<CR>
nmap <leader>' :Marks<CR>
nmap <leader>H :Helptags!<CR>
nmap <leader>M :Maps<CR>
nmap <leader>s :Filetypes<CR>
" }}}

" netrw {{{
"map <C-n> :Lexplore<CR>
" }}}

" Tagbar {{{
nmap <F8> :TagbarToggle<CR>
" }}}

"tnoremap <Esc><Esc><Esc> <C-\><C-n>

nnoremap <leader><leader> <c-^>

nnoremap <leader>tws :ToggleWorkspace<CR>

vmap <leader>ñ :w !xclip<CR>

nnoremap n nzz
nnoremap N Nzz
nnoremap <C-o> <C-o>zz
nnoremap <C-i> <C-i>zz

" close-buffers {{{
nnoremap <silent> Q     :Bdelete menu<CR>
nnoremap <silent> <C-q> :Bdelete menu<CR>
" }}}

" double percentage sign in command mode is expanded {{{
" to directory of current file - http://vimcasts.org/e/14
cnoremap %% <C-R>=expand('%:h').'/'<CR>
" }}}

" git blame {{{
nnoremap <leader>gb :Blame
" }}}

" git-messenger {{{
" Invoke command. 'g' is for call graph, kinda.
nnoremap <silent> <Leader>g :call Cscope('3', expand('<cword>'))<CR>
nmap <Leader>gm <Plug>(git-messenger)
" }}}

" Move visual selection {{{
vnoremap <leader>j :m '>+1gv=gv<CR>
vnoremap <leader>k :m '<-2gv=gv<CR>
" }}}

vnoremap <silent> * :<C-U>
  \let old_reg=getreg('"')<Bar>let old_regtype=getregtype('"')<CR>
  \gvy/<C-R><C-R>=substitute(
  \escape(@", '/\.*$^~['), '\_s\+', '\\_s\\+', 'g')<CR><CR>
  \gV:call setreg('"', old_reg, old_regtype)<CR>

" zoomwintab {{{
nnoremap « :ZoomWinTabIn<CR>
nnoremap » :ZoomWinTabOut<CR>
" }}}

" tabular {{{
nmap <Leader>te :Tabularize /=<CR>
vmap <Leader>te :Tabularize /=<CR>
nmap <Leader>tc :Tabularize /:\zs<CR>
vmap <Leader>tc :Tabularize /:\zs<CR>
nmap <Leader>tp :Tabularize /\|<CR>
vmap <Leader>tp :Tabularize /\|<CR>
" }}}

" vim splits {{{
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
" }}}

" COC {{{
"" Mappings for CoCList
"" Use tab for trigger completion with characters ahead and navigate
"" NOTE: There's always complete item selected by default, you may want to enable
"" no select by `"suggest.noselect": true` in your configuration file
"" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
"" other plugin before putting this into your config
"
"" original tab to auto complete
"" inoremap <silent><expr> <TAB>
""       \ coc#pum#visible() ? coc#pum#next(1) :
""       \ CheckBackspace() ? "\<TAB>" :
""       \ coc#refresh()
"
"" Make <tab> used for trigger completion, completion confirm, snippet expand and jump like VSCode.
"inoremap <silent><expr> <TAB>
"      \ coc#pum#visible() ? coc#_select_confirm() :
"      \ coc#expandableOrJumpable() ? "\<C-r>=coc#rpc#request('doKeymap', ['snippets-expand-jump',''])\<CR>" :
"      \ CheckBackspace() ? "\<TAB>" :
"      \ coc#refresh()
"" CheckBackspace function used in tab mapping
"function! CheckBackspace() abort
"  let col = col('.') - 1
"  return !col || getline('.')[col - 1]  =~# '\s'
"endfunction
"
"" shift+tab to reverse selection order (complete with previous)
"inoremap <expr><S-TAB> coc#pum#visible() ? coc#pum#prev(1) : "\<C-h>"
"" Make <CR> to accept selected completion item or notify coc.nvim to format
"" <C-g>u breaks current undo, please make your own choice
"inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm()
"                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"
"
"" Use <c-space> to trigger completion
"if has('nvim')
"  inoremap <silent><expr> <c-space> coc#refresh()
"else
"  inoremap <silent><expr> <c-@> coc#refresh()
"endif
"
"" GoTo code navigation
"nmap <silent> gd <Plug>(coc-definition)
"nmap <silent> gy <Plug>(coc-type-definition)
"nmap <silent> gi <Plug>(coc-implementation)
"nmap <silent> gr <Plug>(coc-references)
"
"" nmap <silent> gds :call CocAction('jumpDefinition', 'split')<CR>
"" nmap <silent> gdv :call CocAction('jumpDefinition', 'vsplit')<CR>
"" nmap <silent> gdt :call CocAction('jumpDefinition', 'tabe')<CR>
"
"" Use K to show documentation in preview window
"" nnoremap <silent> K :call ShowDocumentation()<CR>
"
"" function! ShowDocumentation()
""   if CocAction('hasProvider', 'hover')
""     call CocActionAsync('doHover')
""   else
""     call feedkeys('K', 'in')
""   endif
"" endfunction
"
"" " Highlight the symbol and its references when holding the cursor
"" autocmd CursorHold * silent call CocActionAsync('highlight')
"
"" Formatting selected code
"xmap <leader>fs  <Plug>(coc-format-selected)
"nmap <leader>fs  <Plug>(coc-format-selected)
"
"augroup mygroup
"  autocmd!
"  " Setup formatexpr specified filetype(s)
"  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
"  " Update signature help on jump placeholder
"  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
"augroup end
"
"" Applying code actions to the selected code block
"" Example: `<leader>aap` for current paragraph
"" map keys for applying code actions
"xmap <leader>a  <Plug>(coc-codeaction-selected)
"nmap <leader>a  <Plug>(coc-codeaction-selected)
"nmap <leader>ac  <Plug>(coc-codeaction-cursor)
"" code actions affect whole buffer
"nmap <leader>as  <Plug>(coc-codeaction-source)
"" Apply the most preferred quickfix action to fix diagnostic on the current line
"nmap <leader>qf  <Plug>(coc-fix-current)
"
"" Use `[g` and `]g` to navigate diagnostics
"" Use `:CocDiagnostics` to get all diagnostics of current buffer in location list
"" nmap <silent> [g <Plug>(coc-diagnostic-prev)
"" nmap <silent> ]g <Plug>(coc-diagnostic-next)
"nmap <silent>dn <Plug>(coc-diagnostic-next)
"nmap <silent>dp <Plug>(coc-diagnostic-prev)
"
"
"" Symbol renaming
"nmap <silent><leader>rn <Plug>(coc-rename)
"" Remap keys for applying refactor code actions
"nmap <silent><leader>r <Plug>(coc-codeaction-refactor)
"xmap <silent><leader>rs  <Plug>(coc-codeaction-refactor-selected)
"nmap <silent><leader>rs  <Plug>(coc-codeaction-refactor-selected)
"
"" Run the Code Lens action on the current line
"nmap <leader>l  <Plug>(coc-codelens-action)
"
"" Map function and class text objects
"" NOTE: Requires 'textDocument.documentSymbol' support from the language server
"xmap if <Plug>(coc-funcobj-i)
"omap if <Plug>(coc-funcobj-i)
"xmap af <Plug>(coc-funcobj-a)
"omap af <Plug>(coc-funcobj-a)
"xmap ic <Plug>(coc-classobj-i)
"omap ic <Plug>(coc-classobj-i)
"xmap ac <Plug>(coc-classobj-a)
"omap ac <Plug>(coc-classobj-a)
"
"
"" Remap <C-f> and <C-b> to scroll float windows/popups
"if has('nvim-0.4.0') || has('patch-8.2.0750')
"  nnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
"  nnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
"  inoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(1)\<cr>" : "\<Right>"
"  inoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(0)\<cr>" : "\<Left>"
"  vnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
"  vnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
"endif
"
"" Use CTRL-S for selections ranges
"" Requires 'textDocument/selectionRange' support of language server
"" nmap <silent> <C-s> <Plug>(coc-range-select)
"" xmap <silent> <C-s> <Plug>(coc-range-select)
"
"" Add `:Format` command to format current buffer
"command! -nargs=0 Format :call CocActionAsync('format')
"
"" Add `:Fold` command to fold current buffer
"command! -nargs=? Fold :call CocAction('fold', <f-args>)
"
"" Add `:OR` command for organize imports of the current buffer
"command! -nargs=0 OR   :call CocActionAsync('runCommand', 'editor.action.organizeImport')
"
"" Add (Neo)Vim's native statusline support
"" NOTE: Please see `:h coc-status` for integrations with external plugins that
"" provide custom statusline: lightline.vim, vim-airline
"set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}
"
"" Mappings for CoCList
"" Show all diagnostics
"nnoremap <silent><nowait> <Leader>ld  :<C-u>CocList diagnostics<cr>
"" Manage extensions
"nnoremap <silent><nowait> <Leader>le  :<C-u>CocList extensions<cr>
"" Show commands
"nnoremap <silent><nowait> <Leader>lc  :<C-u>CocList commands<cr>
"" Find symbol of current document
"nnoremap <silent><nowait> <Leader>lo  :<C-u>CocList outline<cr>
"" Search workspace symbols
"nnoremap <silent><nowait> <Leader>ls  :<C-u>CocList -I symbols<cr>
"" Do default action for next item
"nnoremap <silent><nowait> <Leader>j  :<C-u>CocNext<CR>
"" default action for previous item
"nnoremap <silent><nowait> <Leader>k  :<C-u>CocPrev<CR>
"" Resume latest coc list
"nnoremap <silent><nowait> <Leader>lr  :<C-u>CocListResume<CR>
"" }}}

" vimdiff {{{
if &diff
  nnoremap <leader>c ]c
  nnoremap <leader>C [c
endif
" }}}

" vim inspector {{{
" nmap <LocalLeader>dl :call vimspector#Launch()<CR>
" nmap <LocalLeader>c  <Plug>VimspectorContinue<CR>
" nmap <LocalLeader>rc <Plug>VimspectorRunToCursor<CR>
" nmap <LocalLeader>s  <Plug>VimspectorStop<CR>
" nmap <LocalLeader>r  <Plug>VimspectorRestart<CR>
" nmap <LocalLeader>p  <Plug>VimspectorPause<CR>
" nmap <LocalLeader>tb <Plug>VimspectorToggleBreakpoint<CR>
" nmap <LocalLeader>cb <Plug>VimspectorToggleConditionalBreakpoint<CR>
" nmap <LocalLeader>fb <Plug>VimspectorAddFunctionBreakpoint<CR>
" nmap <LocalLeader>o  <Plug>VimspectorStepOver<CR>
" nmap <LocalLeader>i  <Plug>VimspectorStepInto<CR>
" nmap <LocalLeader>O  <Plug>VimspectorStepOut<CR>
" nmap <LocalLeader>B  <Plug>VimspectorBreakpoints<CR>
" " for normal mode - the word under the cursor
" nmap <LocalLeader>ev <Plug>VimspectorBalloonEval
" " for visual mode, the visually selected text
" xmap <LocalLeader>ev <Plug>VimspectorBalloonEval
" let g:vimspector_enable_mappings = 'HUMAN'
" }}}

" vim-dap Debugger {{{
lua << EOF
  local dap_ok, dap = pcall(require, "dap")
  local dap_ui_ok, dapui = pcall(require, "dapui")
   
  if not (dap_ok and dap_ui_ok) then
    require("notify")("nvim-dap or dap-ui not installed!", "error") -- nvim-notify is a separate plugin, I recommend it too!
    return
  end

  require('dap-python').setup()

  --function file_exists(name)
  --  local io = require('io')
  --  local conf = io.open(name, 'r')
  --  if f~=nil then
  --    io.close(f)
  --    return true
  --  else
  --    return false
  --  end
  --end
  --
  --if file_exists('.vscode/launch.json') then
  --  require('notify')('nvim-dap configured with .vscode/launch.json!', 'info')
  --  require('dap.ext.vscode').load_launchjs()
  --end
  
  dapui.setup({
    icons = { expanded = "▾", collapsed = "▸" },
    mappings = {
      open = "o",
      remove = "d",
      edit = "e",
      repl = "r",
      toggle = "t",
    },
    expand_lines = vim.fn.has("nvim-0.7"),
    layouts = {
      {
        elements = {
          "scopes",
          "watches"
        },
        size = 0.3,
        position = "right"
      },
      {
        elements = {
          "repl",
          "breakpoints"
        },
        size = 0.3,
        position = "bottom",
      },
    },
    floating = {
      max_height = nil,
      max_width = nil,
      border = "single",
      mappings = {
        close = { "q", "<Esc>" },
      },
    },
    windows = { indent = 1 },
    render = {
      max_type_length = nil,
    },
  })
  
  vim.fn.sign_define('DapBreakpoint', { text = '🐞' })
  vim.fn.sign_define('DapStopped', { text = "▶️'" })
  
  -- Start debugging session
  vim.keymap.set("n", "<localleader>ds", function()
    dap.continue()
    --dapui.toggle({})
    dapui.open({})
    vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes("<C-w>=", false, true, true), "n", false) -- Spaces buffers evenly
    require("notify")("Debugger session started", "warn")
  end)
  -- Close debugger and clear breakpoints
  vim.keymap.set("n", "<localleader>de", function()
    dap.clear_breakpoints()
    --dapui.toggle({})
    dapui.close({})
    dap.terminate()
    vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes("<C-w>=", false, true, true), "n", false)
    require("notify")("Debugger session ended", "warn")
  end)
  
  -- Set breakpoints, get variable values, step into/out of functions, etc.
  vim.keymap.set("n", "<localleader>dl", require("dap.ui.widgets").hover)
  vim.keymap.set("n", "<F5>", dap.continue)
  vim.keymap.set('n', '<F6>', dap.pause)
  vim.keymap.set('n', '<F7>', dap.run_to_cursor)
  vim.keymap.set("n", "<F9>", dap.toggle_breakpoint)
  --vim.keymap.set('n', '<LocalLeader>dC', function() dap.set_breakpoint(vim.fn.input '[Condition] > ') end)
  vim.keymap.set("n", "<F10>", dap.step_over)
  vim.keymap.set("n", "<F11>", dap.step_into)
  vim.keymap.set("n", "<S-F11>", dap.step_out)
  --vim.keymap.set('n', '<LocalLeader>db', dap.step_back)
  --vim.keymap.set('n', '<LocalLeader>dd', dap.disconnect)
  --vim.keymap.set('n', '<LocalLeader>dg', dap.session)
  --vim.keymap.set('n', '<LocalLeader>dq', dap.close)
  --vim.keymap.set('n', '<LocalLeader>dr', dap.repl.toggle)
  --vim.keymap.set('n', '<LocalLeader>dx', dap.terminate)
  vim.keymap.set("n", "<localleader>dC", function()
    dap.clear_breakpoints()
    require("notify")("Breakpoints cleared", "warn")
  end)

EOF
" }}}

" reselect {{{
nnoremap vv `[v`]
" }}}

" Vimux: easily interact with tmux from vim {{{

" Prompt for a command to run
map <Leader>r :VimuxPromptCommand<CR>

" Run last command executed by VimuxRunCommand
map <Leader>rr :VimuxRunLastCommand<CR>

" Inspect runner pane
map <Leader>ri :VimuxInspectRunner<CR>

" Close vim tmux runner opened by VimuxRunCommand
map <Leader>re :VimuxCloseRunner<CR>

" Interrupt any command running in the runner pane
map <Leader>rk :VimuxInterruptRunner<CR>

" Zoom the runner pane (use <bind-key> z to restore runner pane)
map <Leader>rz :call VimuxZoomRunner()<CR>

" Clear the terminal screen of the runner pane.
map <Leader>rc :VimuxClearTerminalScreen<CR>

" }}}

" VimTeX: filetype and syntax plugin for LaTeX files {{{
" Viewer options: One may configure the viewer either by specifying a built-in
" viewer method:
"let g:vimtex_view_method = 'zathura'
"let g:vimtex_view_method = 'evince'
let g:vimtex_compiler_latexmk = {
    \ 'build_dir' : '',
    \ 'callback' : 1,
    \ 'continuous' : 1,
    \ 'executable' : 'latexmk',
    \ 'hooks' : [],
    \ 'options' : [
    \   '-verbose',
    \   '-file-line-error',
    \   '-synctex=1',
    \   '-interaction=nonstopmode',
    \   '-shell-escape',
    \ ],
    \}
" }}}

" noremap <F12> <Esc>:syntax sync fromstart<CR>
" inoremap <F12> <C-o>:syntax sync fromstart<CR>
nmap <C-S> <Plug>BujoAddnormal
imap <C-S> <Plug>BujoAddinsert
nmap <C-Q> <Plug>BujoChecknormal
imap <C-Q> <Plug>BujoCheckinsert

" leader guide {{{
nnoremap <silent> <leader> :<c-u>LeaderGuide '<Space>'<CR>
vnoremap <silent> <leader> :<c-u>LeaderGuideVisual '<Space>'<CR>
" }}}

" codeium {{{
let g:codeium_disable_bindings = 1
imap <script><silent><nowait><expr> <C-g> codeium#Accept()
imap <M-n>   <Cmd>call codeium#CycleCompletions(1)<CR>
imap <M-p>   <Cmd>call codeium#CycleCompletions(-1)<CR>
imap <M-x>   <Cmd>call codeium#Clear()<CR>
imap <M-q>   <Cmd>call codeium#Stop()<CR>
imap <M-d>   <Cmd>call codeium#Decrement()<CR>
imap <M-i>   <Cmd>call codeium#Increment()<CR>
"}}}

