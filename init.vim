" LoadConfig {
" foldable block {{{
function! LoadConfig(fileName, ...)
  let BASE = expand("$HOME/vim-settings")
  " Include config files if available
  if filereadable(printf('%s/%s', BASE, a:fileName))
    exec printf('source %s/%s', BASE, a:fileName)
  endif
endfunction
" }}}

:call LoadConfig("tmux.vim")
:call LoadConfig("plugins.vim")
:call LoadConfig("general.vim")
:call LoadConfig("mappings.vim")
:call LoadConfig("autocmd.vim")
" :call LoadConfig("cscope.vim")
